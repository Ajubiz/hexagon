<?php
/* $Id: SalesTypes.php 6998 2014-11-22 02:28:56Z daintree $*/

include('includes/session.inc');
$Title = _('View Rejected Items');
include('includes/header.inc');


echo '<p class="page_title_text">
		<img src="'.$RootPath.'/css/'.$Theme.'/images/inventory.png" title="' . _('Inventory') .
'" alt="" /><b>' . $Title. '</b>
	</p>';

		$rejected_po="SELECT * FROM `rejected_po`";
		$result_rejected_po = DB_query($rejected_po);
		



   echo '<br />
		<table class="selection">';

   $TableHeader = '<tr>
					<th>' . _('PO Number') . '</th>
					<th>' . _('Supplier') . '</th>
					<th>' . _('Item ') . '</th>
					<th>' . _('Batch No ') . '</th>
					<th>' . _('Rejected Date') . '</th>
					<th>' . _('Rejected By') . '</th>
					<th>' . _('Rejected Quantity') . '</th>
					
					</tr>';
	echo $TableHeader;

	$RowCounter = 1;
	$k = 0; //row colour counter

	while ($myrow=DB_fetch_array($result_rejected_po)) {

		if ($k==1){
			echo '<tr class="EvenTableRows">';
			$k=0;
		} else {
			echo '<tr class="OddTableRows">';
			$k++;
		}
		
		$ItemNameSql="SELECT description FROM `stockmaster` WHERE stockid='".$myrow['stockid']."'";
		$result_ItemName = DB_query($ItemNameSql);
		$myrow_ItemName = DB_fetch_array($result_ItemName);
		$ItemName=$myrow_ItemName['description'];
		
		$SuppliersNameSql="SELECT suppname FROM `suppliers` WHERE supplierid='".$myrow['supplierid']."'";
		$result_SuppliersName = DB_query($SuppliersNameSql);
		$myrow_SuppliersName = DB_fetch_array($result_SuppliersName);
		$SuppliersName=$myrow_SuppliersName['suppname'];
		
		
		$format_base = '<td>%s</td>
						<td>%s</td>
						<td>%s</td>
						<td>%s</td>
						<td>%s</td>
						<td>%s</td>
						<td class="number">%s</td>
						';
		
			printf($format_base . '</tr>',
					$myrow['orderno'],
					$SuppliersName,
					$ItemName,
					$myrow['Batch_no'],
					ConvertSQLDate($myrow['rejected_date']),
					$myrow['rejected_by'],
					$myrow['rejected_qty']);
	}
	//end of while loop

 echo '</table>';

include('includes/footer.inc');
?>