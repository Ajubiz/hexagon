<?php
/* $Id: SalesTypes.php 6998 2014-11-22 02:28:56Z daintree $*/

include('includes/session.inc');
$Title = _('View POs Missing COA');
include('includes/header.inc');


 echo'<p class="page_title_text">
		<img src="'.$RootPath.'/css/'.$Theme.'/images/inventory.png" title="' . _('Inventory') .
'" alt="" /><b>' . $Title. '</b>
	</p>';

		$rejected_po="SELECT * FROM purchorders WHERE orderno NOT IN (SELECT purchorders.orderno FROM purchorders, certificator_of_approval WHERE purchorders.orderno = certificator_of_approval.orderno)";
		$result_rejected_po = DB_query($rejected_po);
		
   echo '<br />
		<table class="selection">';

   $TableHeader = '<tr>
					<th>' . _('Order  Number') . '</th>
					<th>' . _('Order Date ') . '</th>
					<th>' . _('Delivery Date') . '</th>	
					<th>' . _('Initiated By') . '</th>
					<th>' . _('Supplier') . '</th>
					</tr>';
	echo $TableHeader;

	$RowCounter = 1;
	$k = 0; //row colour counter

	while ($myrow=DB_fetch_array($result_rejected_po)) {

		if ($k==1){
			echo '<tr class="EvenTableRows">';
			$k=0;
		} else {
			echo '<tr class="OddTableRows">';
			$k++;
		}
		
		
		$SuppliersNameSql="SELECT suppname FROM `suppliers` WHERE supplierid='".$myrow['supplierno']."'";
		$result_SuppliersName = DB_query($SuppliersNameSql);
		$myrow_SuppliersName = DB_fetch_array($result_SuppliersName);
		$SuppliersName=$myrow_SuppliersName['suppname'];
		
		$format_base = '<td>%s</td>
						<td>%s</td>
						<td>%s</td>
						<td>%s</td>
						<td>%s</td>
						';
		
			printf($format_base . '</tr>',
					$myrow['orderno'],
					$myrow['orddate'],
					$myrow['deliverydate'],
					$myrow['initiator'],
					$SuppliersName
					);
	}
	//end of while loop

 echo '</table>';

include('includes/footer.inc');
?>